#include "redis.h"
#include "commands.h"
#include <gtest/gtest.h>
#include <string>

TEST(RedisValue, Construct) {
    RedisValue integer = 10;
    RedisValue string = "abcd";
    RedisValue error = RedisError("Permission denied");

    RedisValue array = std::vector<RedisValue>{integer, string, error};
}

TEST(WriteRedisValue, Int) {
    RedisValue test_1 = 10, test_2 = -5;

    StringWriter writer(1024);
    WriteRedisValue(&writer, test_1);
    writer.flush();

    EXPECT_STREQ(":10\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, test_2);
    writer.flush();

    EXPECT_STREQ(":-5\r\n", writer.result.c_str());
}

TEST(WriteRedisValue, Bulk_String) {
    std::string string_1 = "Check_bulk";
    std::vector<char> bulk_1(10);
    for (size_t i = 0; i < 10; i++) {
        bulk_1[i] = string_1[i];
    }
    RedisValue test_1 = bulk_1;
    StringWriter writer(1024);
    WriteRedisValue(&writer, test_1);
    writer.flush();
    EXPECT_STREQ("$10\r\nCheck_bulk\r\n", writer.result.c_str());

    writer.result.clear();

    std::vector<char> bulk_2;
    RedisValue test_2 = bulk_2;
    WriteRedisValue(&writer, test_2);
    writer.flush();
    EXPECT_STREQ("$0\r\n\r\n", writer.result.c_str());
}

TEST(WriteRedisValue, String) {
    RedisValue test_1 = "String_Test";
    RedisValue test_2 = "";

    StringWriter writer(1024);
    WriteRedisValue(&writer, test_1);
    writer.flush();
    EXPECT_STREQ("+String_Test\r\n", writer.result.c_str());
    writer.result.clear();

    WriteRedisValue(&writer, test_2);
    writer.flush();
    EXPECT_STREQ("+\r\n", writer.result.c_str());
}

TEST(WriteRedisValue, Error) {
    RedisValue test_1 = RedisError("I am not a Redis-server");
    StringWriter writer(1024);
    WriteRedisValue(&writer, test_1);
    writer.flush();
    EXPECT_STREQ("-I am not a Redis-server\r\n", writer.result.c_str()); 
}

TEST(WriteRedisValue, Array) {
    RedisValue integer = 10;
    RedisValue string = "abcd";
    std::vector<RedisValue> test_1(2);
    test_1[0] = integer;
    test_1[1] = string;
    StringWriter writer(1024);

    WriteRedisValue(&writer, test_1);
    writer.flush();
    EXPECT_STREQ("*2\r\n:10\r\n+abcd\r\n", writer.result.c_str());
}

TEST(ReadRedisValue, Int) {
    RedisValue val;
    StringReader reader;

    reader.input = ":10\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(10, boost::get<int64_t>(val));

    reader.input = ":-5\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(-5, boost::get<int64_t>(val));
}

TEST(ReadRedisValue, String) {
    RedisValue val;
    StringReader reader;

    reader.input = "+5\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ("5", boost::get<std::string>(val));
}

TEST(ReadRedisValue, Bulk_String) {
    RedisValue val;
    StringReader reader;

    std::string str = "Bulk_String";
    reader.input = "$11\r\nBulk_String\r\n";
    ReadRedisValue(&reader, &val);
    std::vector<char> tmp = boost::get<std::vector<char>>(val);
    for (int i = 0; i < 11; ++i) {
        EXPECT_EQ(str[i], tmp[i]);
    }
}

TEST(ReadRedisValue, Error) {
    RedisValue val;

    StringReader reader;

    reader.input = "-Error\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(REDIS_ERROR, val.which());
}

TEST(ReadRedisValue, Array) {
    RedisValue val;

    StringReader reader;

    reader.input = "*2\r\n+foo\r\n+bar\r\n";
    ReadRedisValue(&reader, &val);
    std::vector<RedisValue> vector = boost::get<std::vector<RedisValue>>(val);
    EXPECT_EQ("foo", boost::get<std::string>(vector[0]));
    EXPECT_EQ("bar", boost::get<std::string>(vector[1]));
}

TEST(ReadRedisValue, Array_Bulk) {
    RedisValue val;

    StringReader reader;

    reader.input = "*2\r\n$3\r\nget\r\n$1\r\nx\r\n";
    ReadRedisValue(&reader, &val);
    std::vector<RedisValue> vector = boost::get<std::vector<RedisValue>>(val);
    std::string str = "get";
    std::vector<char> tmp = boost::get<std::vector<char>>(vector[0]);
    for (int i = 0; i < 3; ++i) {
        EXPECT_EQ(str[i], tmp[i]);
    }
}

TEST(Name, Set_name) {
    std::unordered_map<std::string, std::string> m;
    Set r(&m);
    std::string res = "set";
    EXPECT_EQ(res, r.name());
}

TEST(Name, Get_name) {
    std::unordered_map<std::string, std::string> m;
    Get r(&m);
    std::string res = "get";
    EXPECT_EQ(res, r.name());
}

TEST(Name, Ping_name) {
    std::unordered_map<std::string, std::string> m;
    Ping r(&m);
    std::string res = "ping";
    EXPECT_EQ(res, r.name());
}
