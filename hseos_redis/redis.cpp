#include "redis.h"
#include <string>

void WriteRedisValue(Writer* w, const RedisValue& value) {
    switch (value.which()) {
        case REDIS_INT: {
            w->write_char(':');
            w->write_int(boost::get<int64_t>(value));
            w->write_crlf();
            break;
        }
        case REDIS_BULK_STRING: {
            std::vector<char> value_vector = boost::get<std::vector<char>>(value);
            w->write_char('$');
            w->write_int(value_vector.size());
            w->write_crlf();
            w->write_raw(&value_vector[0], value_vector.size());
            w->write_crlf();
            break;            
        }
        case REDIS_ERROR: {
            w->write_char('-');
            w->write_string(boost::get<RedisError>(value).msg.c_str());
            w->write_crlf();
            break;            
        }
        case REDIS_STRING: {
            w->write_char('+');
            w->write_string(boost::get<std::string>(value));
            w->write_crlf();
            break;            
        }
        case REDIS_NULL: {
            w->write_char('$');
            w->write_char('-');
            w->write_char('1');
            w->write_crlf();
            break;
        }
        case REDIS_ARRAY: {
            std::vector<RedisValue> value_vector =  boost::get<std::vector<RedisValue>>(value);
            w->write_char('*');
            w->write_int(value_vector.size());
            w->write_crlf();
            for (size_t i = 0; i < value_vector.size(); i++) {
                WriteRedisValue(w, value_vector[i]);
            }
            break;            
        }
        default: {
            throw std::runtime_error("unsupported type");
        }
    }
}

void ReadRedisValue(Reader* r, RedisValue* value) {
    switch(r->read_char()) {
        case ':': {
            *value = r->read_int();
            break;
        }
        case '+': {
            *value = r->read_line();
            break;
        }
        case '-': {
            *value = RedisError(r->read_line());
            break;
        }
        case '$': {
            int64_t len = r->read_int();
            if (len < 0) {
                *value = RedisNull();
            } else {
                *value = r->read_raw(len);
            }
            break;
        }
        case '*': {
            RedisValue val;
            int64_t len = r->read_int();
            if (len < 0) {
                *value = RedisNull();
            } else {
                std::vector<RedisValue> vector;
                for (size_t i = 0; i < len; i++) {
                    ReadRedisValue(r, &val);
                    vector.push_back(val);
                }
                *value = vector;
            }
            break;
        }
        default:
            throw std::runtime_error("invalid redis value");
    }

}
 
