#include "redis.h"
#include "commands.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <iostream>
#include <cstdlib>
#include <unordered_map>
#include <algorithm>

class Server {
public:
    void run_server(std::unordered_map<std::string, std::string>& m);
    void socket_creat();
    void work_with_client(SocketReader reader, SocketWriter writer, std::unordered_map<std::string, std::string>& m);

protected:
    int port_ = 7777;
    int afd;
};
