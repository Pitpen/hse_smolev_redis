#include <iostream>

#include "redis.h"
#include "server.h"

int main(int argc, char* argv[]) {
    std::unordered_map<std::string, std::string> m;
    Server start;
    start.run_server(m);
    return 0;
}
