#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include "redis.h"
#include <string>
#include <iostream>
#include <cstdlib>

class Command {
public:
    virtual ~Command() {}
    virtual std::string name() = 0;
    virtual RedisValue command(RedisValue args) = 0;
};


class Set : public Command {
public:
    virtual std::string name() override;
    virtual RedisValue command(RedisValue args) override;
};

class Get : public Command {
public:
    virtual std::string name() override;
    virtual RedisValue command(RedisValue args) override;
};

RedisValue apply_cmd(const std::vector<Command*>& cmds, RedisValue req);

void test() {
    std::unordered_map<std::string, std::string> m;

    Set set_cmd(&m);
    Get get_cmd(&m);

    Set* p = &set_cmd;
    Command* pp = p;

    pp->name();

    std::vector<Command*> l = { &set_cmd, &get_cmd };

    RedisValue rsp = apply_cmd(l, req);

}



