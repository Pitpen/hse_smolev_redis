#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <string>
#include <iostream>
#include <cstdlib>
#include <unordered_map>

#include "redis.h"

class Command {
public:
    explicit Command(std::unordered_map<std::string, std::string>* m) : m_(m) {}
    virtual std::string name() = 0;
    virtual RedisValue com(RedisValue args) = 0;
protected:
    std::unordered_map<std::string, std::string>* m_;
};


class Set : public Command {
public:
    explicit Set(std::unordered_map<std::string, std::string>* m) : Command(m) {}
    virtual std::string name() override;
    virtual RedisValue com(RedisValue args) override;
};

class Get : public Command {
public:
    explicit Get(std::unordered_map<std::string, std::string>* m) : Command(m) {}
    virtual std::string name() override;
    virtual RedisValue com(RedisValue args) override;
};

class Ping : public Command {
public:
    explicit Ping(std::unordered_map<std::string, std::string>* m) : Command(m) {}
    virtual std::string name() override;
    virtual RedisValue com(RedisValue args) override;
};

/*RedisValue apply_cmd(const std::vector<Command*>& cmds, RedisValue req);

void test() {
    std::unordered_map<std::string, std::string> m;

    Set set_cmd(&m);
    Get get_cmd(&m);

    Set* p = &set_cmd;
    Command* pp = p;

    pp->name();

    std::vector<Command*> l = { &set_cmd, &get_cmd };

    RedisValue rsp = apply_cmd(l, req);

}*/



