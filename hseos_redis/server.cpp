#include "server.h"

RedisValue apply_cmd(const std::vector<Command*>& cmds, RedisValue req) {
    std::vector<RedisValue> args_vector = boost::get<std::vector<RedisValue>>(req);
    std::vector<char> first_arg = boost::get<std::vector<char>>(args_vector[0]);
    std::string name_com = "";
    for (int i = 0; i < first_arg.size(); i++) {
        name_com += first_arg[i];
    }
    for (int i = 0; i < cmds.size(); i++) {
        std::transform(name_com.begin(), name_com.end(), name_com.begin(), ::tolower);
        if (cmds[i]->name() == name_com) {
            std::cout << name_com << std::endl;
            return cmds[i]->com(req);
        }
    }
    return RedisError("Wrong command");
}

void Server::run_server(std::unordered_map<std::string, std::string>& m)
{
    while (1) {
        socket_creat();
        SocketReader reader;
        SocketWriter writer(1024);
        reader.socket = afd;
        writer.socket = afd;
        try {
            work_with_client(reader, writer, m);
        }
        catch (std::exception) {/*std::exception*/
            std::cout << "no client" << std::endl;
        }
        close(afd);
    }
}

void Server::socket_creat() {
    int fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        fprintf(stderr, "socket failed: %s\n", strerror(errno));
        exit(1);
    }
    int opt = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    struct sockaddr_in ba;
    ba.sin_family = AF_INET;
    ba.sin_port = htons(port_);
    ba.sin_addr.s_addr = INADDR_ANY;
    if (bind(fd, (struct sockaddr*) &ba, sizeof(ba)) < 0) {
        fprintf(stderr, "bind failed: %s\n", strerror(errno));
        exit(1);
    }
    if (listen(fd, 16) < 0) {
        fprintf(stderr, "listen failed: %s\n", strerror(errno));
        exit(1);
    }
    struct sockaddr_in aa;
    socklen_t slen = sizeof(aa);
    afd = accept(fd, (struct sockaddr *) &aa, &slen);
    if (afd < 0) {
        fprintf(stderr, "accept failed: %s\n", strerror(errno));
        exit(1);
    }
    printf("ip: %s, port: %d\n", inet_ntoa(aa.sin_addr), ntohs(aa.sin_port));
    close(fd);   
}

void Server::work_with_client(SocketReader reader, SocketWriter writer, std::unordered_map<std::string, std::string>& m) {

    Set set_cmd(&m);
    Get get_cmd(&m);
    Ping ping_cmd(&m);
    std::vector<Command*> cmds = { &set_cmd, &get_cmd, &ping_cmd};
    while(true) {
        RedisValue val;
        ReadRedisValue(&reader, &val);
        RedisValue rsp = apply_cmd(cmds, val);

        WriteRedisValue(&writer, rsp);
        writer.flush();
    }
}