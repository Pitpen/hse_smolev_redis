#include "commands.h"

std::string Set::name() {
    std::string name_ = "set";
    return name_;
}

std::string Get::name() {
    std::string name_ = "get";
    return name_;
}

std::string Ping::name() {
    std::string name_ = "ping";
    return name_;
}

RedisValue Set::com(RedisValue args) {
    std::vector<RedisValue> args_vector = boost::get<std::vector<RedisValue>>(args);
    if (args_vector.size() != 3) {
        return RedisError("Wrong number of arguments for 'set' command");
    }
    std::vector<char> second_arg = boost::get<std::vector<char>>(args_vector[1]);
    std::vector<char> third_arg = boost::get<std::vector<char>>(args_vector[2]);
    std::string string_key = "";
    std::string string_value = "";
    for (int i = 0; i < second_arg.size(); i++) {
        string_key += second_arg[i];
    }
    for (int i = 0; i < third_arg.size(); i++) {
        string_value += third_arg[i];
    }
    (*m_)[string_key] = string_value;
    RedisValue OK = "OK";
    return OK;
}

RedisValue Get::com(RedisValue args) {
    std::vector<RedisValue> args_vector = boost::get<std::vector<RedisValue>>(args);
    if (args_vector.size() != 2) {
        return RedisError("Wrong number of arguments for 'get' command");
    }
    std::vector<char> second_arg = boost::get<std::vector<char>>(args_vector[1]);
    std::string string_key = "";
    for (int i = 0; i < second_arg.size(); i++) {
        string_key += second_arg[i];
    }
    std::string string_result = (*m_)[string_key];
    if (!string_result.size()) {
        return RedisNull();
    } 
    std::vector<char> result;
    for (size_t i = 0; i < string_result.size(); i++) {
        result.push_back(string_result[i]);
    }
    RedisValue redis_result = result;
    return redis_result;
}

RedisValue Ping::com(RedisValue args) {
    std::vector<RedisValue> args_vector = boost::get<std::vector<RedisValue>>(args);
    if (args_vector.size() == 1) {
        RedisValue PONG = "PONG";
        return PONG;
    } else if (args_vector.size() == 2) {
        return args_vector[1];
    } else {
        return RedisError("Wrong number of arguments for 'PING' command");
    }
}
